# Personal Emacs configuration

My first try of a own emacs configuration after using vim for several years and at least a year experience in using spacemacs.

## Current features

- use-package integration
- Evil-mode
- Syntax Highlighting
- More sensetive standard settings
- Disabled many GUI elements
- Automatic bracket matching
- Comment/Uncomment region or line
- Magit integration
- Doom-Theme
- Spacmeacs like SPC prefix
- Evil keybindings:
    - Window splitting
    - Window moving
    - Moving between windows
    - Buffer navigation
    - Multi-term
    - Magit