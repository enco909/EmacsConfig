;;; package --- Basic emacs configuration

;;; Commentary:
;; Requirements: ttf-fira-code, ccls, bear,
;; Considerations: Remap capslock to ctrl via xmodmap

;;; Code:
(setq inhibit-startup-message t)

(menu-bar-mode -1) ; Disable menu bar
(scroll-bar-mode -1) ; Disable scrollbar
(tool-bar-mode -1) ; Disable toolbar
(tooltip-mode -1) ; Disable tooltip
(fringe-mode -1)

;; Set up visual bell
(setq visible-bell t)

;; Bracket highlighting
(show-paren-mode 1)

;; Set font
(set-face-attribute 'default nil :font "Fira Code" :height 100)

;; Set fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Fira Code" :height 110)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Cantarell" :height 120 :weight 'regular)

;; Setup theme
(load-theme 'wombat)

;; Auto follow symlinks (eg. git)
(setq vc-follow-symlinks t)

;; Autocomplete brackets
(electric-pair-mode 1)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Initalize package sources
(require 'package)

 (setq package-archives '(("melpa" . "https://melpa.org/packages/")
;                          ("melpa-stable" . "https://stable.melpa.org/packages/")
                          ("org" . "https://orgmode.org/elpa/")
                          ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("e6f3a4a582ffb5de0471c9b640a5f0212ccf258a987ba421ae2659f1eaa39b09" default))
 '(package-selected-packages
   '(flycheck-plantuml plantuml-mode org-tempo pretty-hydra srefactor ccls flycheck-pos-tip flycheck company-box lsp-treemacs lsp-ivy lsp-ui undo-tree visual-fill-column org-bullets forge evil-magit magit lsp-mode prescient avy counsel-projectile company-prescient ivy-prescient projectile hydra evil-collection evil general doom-themes helpful ivy-rich which-key all-the-icons rainbow-delimiters counsel swiper doom-modeline ivy helm use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Initialize use-package
(unless (package-installed-p 'use-package)
 (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t) ; :ensure t for all packages

;;; Line numbering
(column-number-mode)
(global-display-line-numbers-mode t)

;; Disable linenumbering for some modes
(dolist (mode '(org-mode-hook
		term-mode-hook
		shell-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
	 :map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done)
	 ("C-l" . ivy-alt-done)
	 ("C-j" . ivy-next-line)
	 ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	 ("C-k" . ivy-previous-line)
	 ("C-l" . ivy-done)
	 ("C-d" . ivy-switch-buffer-kill)
	 :map ivy-reverse-i-search-map
	 ("C-k" . ivy-previous-line)
	 ("C-d" . ivy-reverse-i-search-kill))
  :init (ivy-mode 1))

(use-package swiper)

(use-package counsel
  :demand t
  :bind (("<menu>" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         ("C-M-l" . counsel-imenu)
	 ("C-M-j" . counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil)) ;; Don't start searches with ^

(unless (package-installed-p 'all-the-icons)
  (package-install 'all-the-icons)
  (all-the-icons-install-fonts))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 20)))

(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  (load-theme 'doom-oceanic-next ))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package ivy-rich
  :init (ivy-rich-mode 1))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-callable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap desribe-key] . helpful-key))

(use-package general
  :config
  (general-create-definer enco/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")

(enco/leader-keys
  "t" '(:ignore t :which-key "toggles")
  "tt" '(counsel-load-theme :which-key "choose theme")))

(general-unbind
  "C-x C-e")
(general-define-key
 :keymaps '(normal insert visual emacs)
 "C-x C-e e" 'eval-last-sexp
 "C-x C-e b" 'eval-buffer)

(defun enco/evil-hook ()
  "Initializing hook for 'evil'."
  (defvar evil-emacs-state-modes)
  (dolist (mode '(eshell-mode
		  term-mode))
    (add-to-list 'evil-emacs-state-modes mode)))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  (evil-mode 1)
  :hook (evil-mode . enco/evil-hook)
  :config
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package undo-tree
  :config
  (global-undo-tree-mode)
  (evil-set-undo-system 'undo-tree))

(use-package avy
  :config
  (enco/leader-keys
  "SPC" '(evil-avy-goto-char :which-key "go to char")))

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(enco/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))

(use-package prescient
  :hook
  (after-init . prescient-persist-mode))

(use-package ivy-prescient
  :after counsel
  :config
  (ivy-prescient-mode 1)
  (defvar perscient-filter-method)
  (setq prescient-sort-length-enable nil)
  (setq perscient-filter-method '(literal regexp fuzzy))
  (setq ivy-prescient-retain-classic-highlighting t))

(use-package company-prescient
  :after company
  :config
  (company-prescient-mode 1))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom (projectile-completion-system 'ivy)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "/home/enrico/Documents/Sources/")
    (setq projectile-project-search-path '("/home/enrico/Documents/Sources/")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; Need setup for tokens on gitlab
(use-package forge)

(defun enco/org-mode-setup ()
  "Basic setup of 'org-mode'."
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(defun enco/org-font-setup ()
  "Setup of 'org-mode' fonts."
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.8)
		  (org-level-2 . 1.7)
		  (org-level-3 . 1.6)
		  (org-level-4 . 1.5)
		  (org-level-5 . 1.4)
		  (org-level-6 . 1.3)
		  (org-level-7 . 1.2)
		  (org-level-8 . 1.1)))
  (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . enco/org-mode-setup)
  :config
  (enco/org-font-setup)
  :custom
  (org-ellipsis " ▾" org-hide-emphasis-markers t)

  ;; Agenda
  (org-agenda-files
   '("~/Dokumente/OrgFiles/Tasks.org"
     "~/Dokumente/OrgFiles/Birthdays.org"))

  (org-agenda-start-with-log-mode t)
  (org-log-done 'time)
  (org-log-into-drawer t)
  (org-agenda-restore-windows-after-quit t)

  ;; TODO: Fit to my preferences
  ;; Configure custom agenda views
  (org-todo-keywords
   '((:startgroup)
     ;; Put mutually exclusive tags here
     (:endgroup)
     ("@errand" . ?E)
     ("@home" . ?H)
     ("@work" . ?W)
     ("agenda" . ?a)
     ("planning" . ?p)
     ("publish" . ?p)
     ("batch" . ?b)
     ("note" . ?n)
     ("idea" . ?i)))
  
  (org-agenda-custom-commands
   '(("d" "Dashboard"
     ((agenda "" ((org-deadline-warning-days 7)))
      (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
      (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))

    ("W" "Work Tasks" tags-todo "+work-email")

    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))

    ("w" "Workflow Status"
     ((todo "WAIT"
            ((org-agenda-overriding-header "Waiting on External")
             (org-agenda-files org-agenda-files)))
      (todo "REVIEW"
            ((org-agenda-overriding-header "In Review")
             (org-agenda-files org-agenda-files)))
      (todo "PLAN"
            ((org-agenda-overriding-header "In Planning")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "BACKLOG"
            ((org-agenda-overriding-header "Project Backlog")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "READY"
            ((org-agenda-overriding-header "Ready for Work")
             (org-agenda-files org-agenda-files)))
      (todo "ACTIVE"
            ((org-agenda-overriding-header "Active Projects")
             (org-agenda-files org-agenda-files)))
      (todo "COMPLETED"
            ((org-agenda-overriding-header "Completed Projects")
             (org-agenda-files org-agenda-files)))
      (todo "CANC"
            ((org-agenda-overriding-header "Cancelled Projects")
             (org-agenda-files org-agenda-files)))))))

  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
     (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD (h)" "|" "COMPLETED (c)" "CANC(k@)"))))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun enco/org-mode-visual-fill ()
  "Setup for 'visual-fill-column' for 'org-mode'."
  (defvar visual-fill-column-width)
  (defvar visual-fill-column-center-text)
  (setq visual-fill-column-width 150
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

;; TODO: Look into Org-wild-notifier or in general how to get notified efficently
;; TODO Look into stripe-buffer

(use-package visual-fill-column
  :hook (org-mode . enco/org-mode-visual-fill))

(general-define-key
 :keymaps '(normal visual emacs insert)
 "C-c aa" 'org-agenda
 "C-c al" 'org-agenda-list)

;; Lsp-Mode
;; TODO: Maybe seperate language config more from lsp config
;; TODO: Review config
;; TODO: Integration with projectile????
;; TODO: Make familiar with keybindings->maybe system crafters first and second video on it
(use-package lsp-mode
  :hook ((c-mode . lsp)
	 (c++-mode . lsp)
	 (lsp-mode . lsp-enable-which-key-integration))
  :commands
  (lsp lsp-deferred)
  :config
  (define-key lsp-mode-map (kbd "C-c l") lsp-command-map)
  :custom
  (lsp-keymap-prefix "C-c l")
  (lsp-file-watch-threshold 15000))

(use-package lsp-ui
  :commands (lsp-ui-mode)
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  :custom
  (lsp-ui-doc-enable nil)
  (lsp-ui-doc-delay 0.5))

(use-package lsp-ivy
  :commands
  (lsp-ivy-workspace-symbol))

(use-package lsp-treemacs
  :commands lsp-treemacs-errors-list)

;; Company
(use-package company
  :bind ("M-/" . company-complete-common-or-cycle)
  :init (add-hook 'after-init-hook 'global-company-mode)
  :config
  (setq company-show-quick-access t
	company-minimum-prefix-length 1
	company-idle-delay 0.5
	company-backends
	'((company-files
	   company-keywords
	   company-capf
	   company-yasnippet)
	  (company-abbrev company-dabbrev))))

(use-package company-box
  :after company
  :hook (company-mode . company-box-mode))

;; Flycheck
(use-package flycheck
  :init (global-flycheck-mode)
  :custom
  (flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list)
  (flycheck-indication-mode nil))

(use-package flycheck-pos-tip
  :after flycheck
  :config (flycheck-pos-tip-mode))

;; CCLS (Langugage Server C/C++)
(use-package ccls
  :config
  :hook ((c-mode c++-mode) . (lambda() (require 'ccls) (lsp)))
  :custom
  (ccls-executable "/usr/bin/ccls")
  (ccls-initialization-options '(:index (:comments 2) :completion (:detailedLabel t))))

(use-package srefactor
  :config
  (semantic-mode 1)
  (define-key c-mode-map (kbd "M-RET") 'srefactor-refactor-at-point)
  (define-key c++-mode-map (kbd "M-RET") 'srefactor-refactor-at-point)
  (enco/leader-keys
    :keymap '(c-mode-map c++-mode-map)
    "rr" '(srefactor-refactor-at-point)))

;; Build system
;; TODO: This functions not working completly
(defun bearmake-compile-command ()
  "Bear make compile command."
  (interactive)
  (set (make-local-variable 'compile-command)
       (concat "bear make "
  	       (if buffer-file-name
  		   (shell-quote-argument
  		    (file-name-sans-extension buffer-file-name)))))
  (call-interactively 'compile))

(defun make-compile-command ()
  "Make compile command."
  (interactive)
  (set (make-local-variable 'compile-command)
       (concat "make "
  	       (if buffer-file-name
  		   (shell-quote-argument
  		    (file-name-sans-extension buffer-file-name)))))
  (call-interactively 'compile))

;; CMake
;; TODO: This functions not working completly
(defun cmake-compile-command ()
  "CMake compile command."
  (interactive)
  (set (make-local-variable 'compile-command)
       (concat "cmake -H. -Bbuild -DCMAKE_EXPORT_COMPILE_COMMANDS=1 "
  	       (if buffer-file-name
  		   (shell-quote-argument
  		    (file-name-sans-extension buffer-file-name)))))
  (call-interactively 'compile))

(defun cmake-build-command ()
  "CMake build command."
  (interactive)
  (set (make-local-variable 'compile-command)
       (concat "cmake --build build "
  	       (if buffer-file-name
  		   (shell-quote-argument
  		    (file-name-sans-extension buffer-file-name)))))
  (call-interactively 'compile))

(defun cmake-install-command ()
  "CMake install command."
  (interactive)
  (set (make-local-variable 'compile-command)
       (concat "cmake install "
  	       (if buffer-file-name
  		   (shell-quote-argument
  		    (file-name-sans-extension buffer-file-name)))))
  (call-interactively 'compile))

;; Hydra / Build
(use-package pretty-hydra)

;; TODO Usage??
(pretty-hydra-define hydra-build
  (:hint nil :title "Build (in root)")
  ("CMake"
   (("c" cmake-compile-command "CMake compile")
    ("b" cmake-build-command "CMake build")
    ("i" cmake-install-command "CMake install"))
   "Others"
   (("B" bearmake-compile-command "Bear Make")
    ("M" make-compile-command "Make"))))

(enco/leader-keys
  "b" '(hydra-build/body :which-key "Build (in root)"))

;; C++ Indentions
(defun enco-c++-hook ()
  (c-set-offset 'substatement-open 0)
  (setq c-basic-offset 4)
  (c-set-offset 'statement-cont 0)
  (c-set-offset 'brace-list-open 0))
(add-hook 'c++-mode-hook 'enco-c++-hook)

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;; PlantUml
(use-package plantuml-mode
  :config
  (setq plantuml-executable-path "/usr/bin/plantuml")
  (setq plantuml-default-exec-mode 'executable)
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
)
(use-package flycheck-plantuml
  :config
  (with-eval-after-load 'flycheck
    (require 'flycheck-plantuml)
    (flycheck-plantuml-setup))
  (setq flycheck-plantuml-executable "/usr/bin/plantuml"))

;; INFO: Part 8 at 38:00

(provide 'init)

;;; init.el ends here
